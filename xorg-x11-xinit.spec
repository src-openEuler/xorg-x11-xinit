Name:         xorg-x11-xinit
Version:      1.4.3
Release:      1
Summary:      X.Org X11 X Window System xinit startup scripts
License:      MIT
URL:          https://www.x.org
Source0:      https://www.x.org/releases/individual/app/xinit-%{version}.tar.xz
Source1:      xinitrc-common
Source2:      xinitrc
Source3:      Xclients
Source4:      Xmodmap
Source5:      Xresources
Source6:      Xsession
Source7:      localuser.sh
Source8:      xinit-compat.desktop
Source9:      xinit-compat

Patch0000:    xinit-1.0.2-client-session.patch
Patch0001:    0003-startx-Make-startx-auto-display-select-work-with-per.patch
Patch0002:    xinit-1.3.4-set-XORG_RUN_AS_USER_OK.patch

BuildRequires: make gcc
BuildRequires: pkgconfig(x11)
BuildRequires: pkgconfig(xproto) >= 7.0.22
BuildRequires: /usr/bin/mcookie

Requires:     xorg-x11-xauth coreutils xhost
Provides:     xinit = %{version} %{name}-session = %{version}-%{release}
Obsoletes:    %{name}-session < %{version}-%{release}

%description
X.Org X11 X Window System xinit startup scripts.

%package_help

%prep
%autosetup -n xinit-%{version} -p1

%build
%configure
%make_build

%install
%make_install
install -p -m644 -D %{SOURCE8} $RPM_BUILD_ROOT%{_datadir}/xsessions/xinit-compat.desktop
install -D -p -m 644 %{SOURCE1} $RPM_BUILD_ROOT%{_sysconfdir}/X11/xinit/xinitrc-common
for script in %{SOURCE2} %{SOURCE3} %{SOURCE6} ; do
     install -p -m 755 $script $RPM_BUILD_ROOT%{_sysconfdir}/X11/xinit/${script##*/}
done
install -p -m 644 %{SOURCE4} $RPM_BUILD_ROOT%{_sysconfdir}/X11/Xmodmap
install -p -m 644 %{SOURCE5} $RPM_BUILD_ROOT%{_sysconfdir}/X11/Xresources
install -D -p -m 755 %{SOURCE7} $RPM_BUILD_ROOT%{_sysconfdir}/X11/xinit/xinitrc.d/localuser.sh
install -d %{buildroot}%{_sysconfdir}/X11/xinit/Xclients.d
install -D -p -m 755 %{SOURCE9} $RPM_BUILD_ROOT%{_libexecdir}/xinit-compat

rm -f %{buildroot}%{_datadir}/xsessions/xinit-compat.desktop

%files
%license COPYING
%doc README.md ChangeLog
%{_bindir}/*
%{_sysconfdir}/X11/xinit/*
%config(noreplace) %{_sysconfdir}/X11/*
%{_sysconfdir}/X11/xinit/xinitrc.d/*
%{_libexecdir}/xinit-compat

%files help
%{_mandir}/man1/startx.1*
%{_mandir}/man1/xinit.1*

%changelog
* Mon Jan 06 2025 Funda Wang <fundawang@yeah.net> - 1.4.3-1
- update to 1.4.3

* Fri May 10 2024 peijiankang <peijiankang@kylinos.cn> - 1.4.2-2
- Support UKUI and kiran desktop

* Sat Sep 02 2023 wulei <wu_lei@hoperun.com> - 1.4.2-1
- Update to 1.4.2

* Wed Jan 12 2022 yaoxin <yaoxin30@huawei.com> - 1.4.1-1
- Upgrade xorg-x11-xinit to 1.4.1

* Thu Jul 29 2021 zhangshaoning <zhangshaoning@uniontech.com> - 1.4.0-7
- Remove harmless file: xinit-compat.desktop

* Thu Jul 30 2020 douyan <douyan@kylinos.cn> - 1.4.0-6
- update spec, add xinitrc,Xsession file

* Wed Jan 15 2020 openEuler Buildteam <buildteam@openeuler.org> - 1.4.0-5
- update spec

* Fri Oct 25 2019 Lijin Yang <yanglijin@huawei.com> - 1.4.0-4
- Package init
